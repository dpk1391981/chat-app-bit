const express = require("express");
const path = require("path");
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const ROUTES = {
  users: require("./routes/api/users"),
  auth: require("./routes/api/auth"),
  chatroom: require("./routes/api/chatroom"),
  friends: require("./routes/api/friends"),
};

//Define routes
app.use("/api/users", ROUTES["users"]);
app.use("/api/auth", ROUTES["auth"]);
app.use("/api/chatroom", ROUTES["chatroom"]);
app.use("/api/friend", ROUTES["friends"]);

console.log(path.resolve(__dirname, "client", "build", "index.html"));

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.resolve(__dirname, "client/build")));
  app.get("*", function (req, res) {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

module.exports = app;
