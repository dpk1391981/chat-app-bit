const express = require("express");
const router = express.Router();
const FriendRequest = require("../../models/FriendRequest");
const Friend = require("../../models/Friend");
const { body, validationResult } = require("express-validator");
const auth = require("../../handlers/auth");

/**
 * @route POST api/users
 * @desc Register users
 * @access Public
 */
router.post(
  "/request",
  [
    auth,
    body("friends", "friend id is required!!!").not().isEmpty(),
    body("status", "status is required!!!").not().isEmpty(),
  ],
  async (req, res) => {
    try {
      const error = validationResult(req);
      if (!error.isEmpty()) {
        return res.status(400).json({ errors: error.array() });
      }

      const { friends, status } = req.body;
      let friend = await FriendRequest.findOne({
        sendTo: friends,
        sendBy: req.user.id,
      });

      if (friend && friend.status == "approved") {
        return res
          .status(400)
          .json({ errors: [{ msg: "you are already in friend list" }] });
      }

      if (friend && friend.status == "pending") {
        return res
          .status(400)
          .json({ errors: [{ msg: "already sent request" }] });
      }

      let saveFrindsParams = {
        sendTo: friends,
        sendBy: req.user.id,
        status,
      };

      friend = new FriendRequest(saveFrindsParams);
      await friend.save();
      res.json({ msg: "Request Sent" });
    } catch (error) {
      console.log(`Error @friends friends: ${error.message}`);
      res.status(500).send("Server Error");
    }
  }
);

/**
 * @route POST api/chatroom
 * @desc create chat room
 * @access private
 */
router.get("/request/list", [auth], async (req, res) => {
  try {
    console.log(req.params);
    let friends = await FriendRequest.find({})
      .populate("sendBy", ["name", "_id"])
      .populate("sendTo", ["name", "_id"])
      .lean()
      .exec();

    let pendingList = friends.filter((u) => u.status == "pending");

    return res.status(200).json({
      requestTo: pendingList
        .map((e) => {
          return {
            _id: e._id,
            user: e["sendTo"],
          };
        })
        .filter((e) => e.user._id != req.user.id),
      requestBy: pendingList
        .map((e) => {
          return {
            _id: e._id,
            user: e["sendBy"],
          };
        })
        .filter((e) => e.user._id != req.user.id),
    });
  } catch (error) {
    console.log(`Error @get friends: ${error}`);
    res.status(500).send("Server Error");
  }
});

/**
 * @route POST api/users
 * @desc Register users
 * @access Public
 */
router.get("/:fid/accept", [auth], async (req, res) => {
  try {
    let fid = req.params.fid;
    if (!fid) {
      return res.status(404).json({ errors: "friend request id not found" });
    }

    let friend = await FriendRequest.findById(fid);
    if (!friend) {
      return res.status(404).json({ errors: [{ msg: "No list avail" }] });
    }

    friend.status = "approved";
    await friend.save();

    let friendList = await Friend.findOne({ user: req.user.id });

    let friensTo = friendList
      ? [...friend.sendTo, ...friendList.sendTo]
      : [friend.sendTo];

    let friensBy = friendList
      ? [...friend.sendBy, ...friendList.sendBy]
      : [friend.sendBy];

    let friendSave = new Friend({
      user: friend.sendBy,
      friendRequestId: fid,
      friends: friensTo,
    });
    await friendSave.save();

    new Friend({
      user: friend.sendTo,
      friendRequestId: fid,
      friends: friensBy,
    }).save();

    res.json({ msg: "Accepted" });
  } catch (error) {
    console.log(`Error @friends friends: ${error.message}`);
    res.status(500).send("Server Error");
  }
});

/**
 * @route POST api/chatroom
 * @desc create chat room
 * @access private
 */
router.get("/list", [auth], async (req, res) => {
  try {
    let friends = await Friend.find({ user: req.user.id })
      .populate("friends", ["name", "_id"])
      .lean()
      .exec();

    return res.status(200).json(friends);
  } catch (error) {
    console.log(`Error @get friends: ${error}`);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
