const app = require("./app");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
dotenv.config();
const connectDB = require("./models/db");

connectDB(mongoose);
require("./models/User");
require("./models/Message");

const PORT = process.env.PORT || 9999;
const server = app.listen(PORT, () => {
  console.log(`Server running @ http://localhost:${PORT}/`);
});

const Message = mongoose.model("Message");
const User = mongoose.model("User");

const io = require("socket.io")(server);

io.use(async (socket, next) => {
  try {
    const token = socket.handshake.query.token;
    const { user } = await jwt.verify(token, process.env.JWTSECRET);
    socket.userId = user.id;
    console.log(`socket.userId: ${socket.userId}`);
    next();
  } catch (error) {}
});

//client socket config

io.on("connection", (socket) => {
  console.log(`Connected: ${socket.userId}`);

  socket.on("disconnect", () => {
    console.log(`Disconnected: ${socket.userId}`);
  });

  socket.on("joinChat", ({ userChatId }) => {
    socket.join(userChatId);
    console.log("A user joined chat: " + userChatId);
  });

  socket.on("leaveChat", ({ userChatId }) => {
    socket.leave(userChatId);
    console.log("A user left chat: " + userChatId);
  });
  socket.on("chatMessage", async ({ userChatId, message }) => {
    console.log(`inside chat room`);
    console.log(`userChatId: ${userChatId}`);
    console.log(message);
    if (message.trim().length > 0) {
      const user = await User.findOne({ _id: socket.userId });
      const newMessage = new Message({
        friendId: userChatId,
        user: socket.userId,
        message,
      });

      io.to(userChatId).emit("newMessage", {
        message,
        name: user.name,
        userId: socket.userId,
      });
      await newMessage.save();
    }
  });
});
