const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Friend = new Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: "User is required",
      ref: "User",
    },
    friendRequestId: {
      type: String,
    },
    friends: [
      {
        type: mongoose.Schema.Types.ObjectId,
        required: "friend id is requried",
        ref: "User",
      },
    ],
  },
  { timestamps: true }
);

module.exports = Message = mongoose.model("Friend", Friend);
