import React from "react";
import { Route, Redirect } from "react-router-dom";

const token = localStorage.getItem("CC_Token");
const PrivateRoutes = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      !token ? (
        <Redirect to="/login" />
      ) : (
        <Component {...props} {...rest} exact />
      )
    }
  />
);

export default PrivateRoutes;
