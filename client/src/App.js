import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import React, { useState, useEffect } from "react";
import Index from "./component/Pages/Index";
import Login from "./component/Pages/Login";
import Register from "./component/Pages/Register";
import Dashboard from "./component/Pages/Dashboard";
import UserChat from "./component/Pages/UserChat";
import socketIOClient from "socket.io-client";
import makeToast from "./Toaster";

function App() {
  const [socket, setSocket] = useState(null);

  const setUpSokcet = () => {
    const token = localStorage.getItem("CC_Token");
    if (token && !socket) {
      const newSocket = socketIOClient("http://localhost:9999", {
        query: {
          token: token,
        },
      });

      newSocket.on("disconnect", () => {
        setSocket(null);
        setTimeout(setUpSokcet, 3000);
        makeToast("error", "Socket Disconnected");
      });

      newSocket.on("connect", () => {
        makeToast("success", "socket connected");
      });

      setSocket(newSocket);
    }
  };

  useEffect(() => {
    console.log(`init connect`);
    setUpSokcet();
  }, []);

  return (
    <Router>
      <Switch>
        <Route path="/" component={Index} exact></Route>
        <Route
          path="/login"
          render={() => <Login setUpSokcet={setUpSokcet} />}
          exact
        ></Route>
        <Route
          path="/register"
          render={() => <Register socket={socket} />}
          exact
        ></Route>
        <Route
          path="/dashboard"
          render={() => <Dashboard socket={socket} />}
          exact
        ></Route>
        <Route
          path="/chat/user/:id"
          render={() => <UserChat socket={socket} />}
          exact
        ></Route>
      </Switch>
    </Router>
  );
}

export default App;
