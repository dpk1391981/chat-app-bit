import React, { useState, useEffect } from "react";

import makeToast from "../../Toaster";
import { withRouter } from "react-router-dom";
import axios from "axios";
import UsersList from "../Pages/UsersList";

const Dashboard = (props) => {
  useEffect(() => {
    const token = localStorage.getItem("CC_Token");
    if (!token) {
      props.history.push("/login");
    } else {
      props.history.push("/dashboard");
    }
    // eslint-disable-next-line
  }, [0]);
  const chatroomNameRef = React.createRef();
  const [usersList, setUsers] = useState([]);
  const [approvedList, setApproved] = useState([]);
  const [pendingRequest, setPendingRequest] = useState([]);
  const [friendRequest, setFriendRequest] = useState([]);

  const getAllUser = async () => {
    console.log(`token: ${localStorage.getItem("CC_Token")}`);
    let { data } = await axios.get("/api/users/all", {
      headers: {
        "x-auth-token": localStorage.getItem("CC_Token"),
      },
    });

    setUsers(data["users"]);
    setPendingRequest(data["pendingRequest"]);
    setFriendRequest(data["friendRequest"]);
    setApproved(data["friends"]);
  };

  useEffect(async () => {
    async function fetchData() {
      await getAllUser();
    }
    await fetchData();
  }, []);

  const sendRequest = (id) => {
    axios
      .post(
        "/api/friend/request",
        {
          friends: id,
          status: "pending",
        },
        {
          headers: {
            "x-auth-token": localStorage.getItem("CC_Token"),
          },
        }
      )
      .then(async () => {
        makeToast("success", `Request sent`);
        await getAllUser();
      })
      .catch((err) => {});
  };

  const acceptRequest = (id) => {
    axios
      .get(`/api/friend/${id}/accept`, {
        headers: {
          "x-auth-token": localStorage.getItem("CC_Token"),
        },
      })
      .then(async (response) => {
        console.log(response.data);
        makeToast("success", `Accepted`);
        await getAllUser();
      })
      .catch((err) => {});
  };

  return (
    <>
      <UsersList
        usersList={usersList}
        title="Users in List"
        type="users"
        chRef={chatroomNameRef}
        sendRequest={sendRequest}
      />
      <UsersList
        usersList={approvedList[0] ? approvedList[0]["friends"] : []}
        title="Your Friends List"
        type="friends"
      />
      <UsersList
        usersList={friendRequest}
        title="Friend Request"
        type="friendrequest"
        acceptRequest={acceptRequest}
      />
      <UsersList
        usersList={pendingRequest}
        title="Pending Request"
        type="pending"
      />
    </>
  );
};

export default withRouter(Dashboard);
