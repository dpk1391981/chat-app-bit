import React from "react";
import Layout from "../Layout";
import { Link, withRouter } from "react-router-dom";
const UsersList = ({ usersList, title, type, sendRequest, acceptRequest }) => {
  let buttonFun = (type, el) => {
    let button = "";
    switch (type) {
      case "users":
        button = (
          <button
            className="btn_join"
            onClick={() => {
              sendRequest(el._id);
            }}
          >
            Request
          </button>
        );
        break;

      case "friends":
        button = (
          <Link to={`/chat/user/${el._id}`} className="btn_join">
            SEND
          </Link>
        );
        break;

      case "friendrequest":
        button = (
          <button
            className="btn_join"
            onClick={() => {
              acceptRequest(el._id);
            }}
          >
            Accept
          </button>
        );
        break;

      case "pending":
        button = <button className="btn_join">Pending</button>;
        break;
    }

    return button;
  };

  console.log(usersList);

  return (
    <Layout>
      <div id="signup">
        <div className="header">
          <h3>{title}</h3>
        </div>

        <div className="inputs">
          <div>
            <table className="chat_tbl" border="0" style={{ width: "100%" }}>
              <tbody>
                {usersList.length > 0 &&
                  usersList.map((el) => (
                    <tr key={el._id} test={el._id}>
                      <td style={{ width: "100%" }}>
                        <span className="user_name">
                          {el.name ? el.name : el.user.name}
                        </span>
                      </td>
                      <td>{buttonFun(type, el)}</td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default withRouter(UsersList);
