import React from "react";
import makeToast from "../../Toaster";
import axios from "axios";
import Layout from "../Layout";
import { Link, withRouter, Redirect } from "react-router-dom";

function Login({ history, setUpSokcet }) {
  React.useEffect(() => {
    const token = localStorage.getItem("CC_Token");
    if (!token) {
      history.push("/login");
    } else {
      history.push("/dashboard");
    }
    // eslint-disable-next-line
  }, [0]);

  const emailRef = React.createRef();
  const passwordRef = React.createRef();
  const loginUser = () => {
    const email = emailRef.current.value;
    const password = passwordRef.current.value;

    axios
      .post("/api/auth", {
        email,
        password,
      })
      .then((response) => {
        // makeToast("success", response.data.message);
        localStorage.setItem("CC_Token", response.data.token);
        history.push("/dashboard");
        // setUpSokcet();
      })
      .catch((err) => {
        // console.log(err);
        // if (
        //   err &&
        //   err.response &&
        //   err.response.data &&
        //   err.response.data.message
        // )
        //   console.log(err.response.data.errors);
        // err.response.data.errors.forEach((e) => {
        //   makeToast("error", e.msg);
        // });
      });
  };
  return (
    <Layout>
      <div id="signup">
        <div className="header">
          <h3>Login</h3>
        </div>

        <div className="sep"></div>

        <div className="inputs">
          <input
            type="email"
            name="email"
            placeholder="e-mail"
            ref={emailRef}
          />
          <input
            type="password"
            name="password"
            placeholder="Password"
            ref={passwordRef}
          />
          <button onClick={loginUser} id="submit">
            Login in
          </button>
          <div className="checkboxy">
            Not register? <Link to="/register">Register </Link>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default withRouter(Login);
