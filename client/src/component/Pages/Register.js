import React from "react";
import axios from "axios";
import makeToast from "../../Toaster";
import { Link, withRouter } from "react-router-dom";
import Layout from "../Layout";

const Register = ({ history }) => {
  const nameRef = React.createRef();
  const emailRef = React.createRef();
  const passwordRef = React.createRef();

  const registerUser = () => {
    const name = nameRef.current.value;
    const email = emailRef.current.value;
    const password = passwordRef.current.value;

    axios
      .post("/api/users", {
        name,
        email,
        password,
      })
      .then((response) => {
        console.log("response");
        console.log(response);
        makeToast("success", "Register Successfully Done!!!!");
        history.push("/login");
      })
      .catch((err) => {
        // console.log(err);
        if (
          err &&
          err.response &&
          err.response.data &&
          err.response.data.message
        )
          console.log("err response");
        console.log(err.response);
        err.response.data.errors.forEach((e) => {
          makeToast("error", e.msg);
        });
      });
  };

  return (
    <Layout>
      <div id="signup">
        <div className="header">
          <h3>Sign Up</h3>

          <p>Want to start a chat ? Please provide some info.</p>
        </div>

        <div className="sep"></div>

        <div className="inputs">
          <input
            type="text"
            name="name"
            placeholder="Your name"
            autofocus
            ref={nameRef}
          />
          <input
            type="email"
            name="email"
            placeholder="e-mail"
            ref={emailRef}
          />
          <input
            type="password"
            name="password"
            placeholder="Password"
            ref={passwordRef}
          />
          <button onClick={registerUser} id="submit">
            Sign Up
          </button>
          <div className="checkboxy">
            Already signup? <Link to="/login">Login </Link>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default withRouter(Register);
