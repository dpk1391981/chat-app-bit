import React, { useEffect, useState, useRef } from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";

function UserChat({ match, socket, history }) {
  const userChatId = match.params.id;
  useEffect(() => {
    const token = localStorage.getItem("CC_Token");
    if (!token) {
      history.push("/login");
    } else {
      history.push("/chat/user/" + userChatId);
    }
    // eslint-disable-next-line
  }, [0]);
  console.log(socket);
  console.log(`userChatId: ${userChatId}`);
  const [messages, setMessages] = useState([]);
  const [chatRoomName, setChatRoomName] = useState("User Name");
  const messageRef = useRef();
  const [userId, setUserId] = useState("");

  const sendMessage = () => {
    if (socket) {
      console.log(`Socekt intiationslist`);
      socket.emit("chatMessage", {
        userChatId,
        message: messageRef.current.value,
      });

      messageRef.current.value = "";
    }
  };

  useEffect(() => {
    const token = localStorage.getItem("CC_Token");
    if (token) {
      const payload = JSON.parse(atob(token.split(".")[1]));
      setUserId(payload.id);
    }
    if (socket) {
      console.log(`insideNew messg`);
      socket.on("newMessage", (message) => {
        const newMessages = [...messages, message];
        console.log(`newMessages`);
        console.log(newMessages);
        setMessages(newMessages);
      });
    }
  }, [messages]);

  useEffect(async () => {
    let { data } = await axios.get(`/api/users/chat/${userChatId}`, {
      headers: {
        "x-auth-token": localStorage.getItem("CC_Token"),
      },
    });

    setChatRoomName(data["name"]);
    if (socket) {
      socket.emit("joinChat", {
        userChatId,
      });
    }

    return () => {
      //Component Unmount
      if (socket) {
        socket.emit("leaveChat", {
          userChatId,
        });
      }
    };
    //eslint-disable-next-line
  }, []);

  console.log(`messages`);
  console.log(messages);
  return (
    <div className="chatroomPage">
      <div className="chatroomSection">
        <div className="cardHeader">{chatRoomName}</div>
        <div className="chatroomContent">
          {messages.map((message, i) => (
            <div key={i} className="message">
              <span
                className={
                  userId === message.userId ? "ownMessage" : "otherMessage"
                }
              >
                {message.name}:
              </span>{" "}
              {message.message}
            </div>
          ))}
        </div>
        <div className="chatroomActions">
          <div>
            <input
              type="text"
              name="message"
              placeholder="Say something!"
              ref={messageRef}
            />
          </div>
          <div>
            <button className="join" onClick={sendMessage}>
              Send
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withRouter(UserChat);
